<?php


namespace Smartosc\Article\Block;


use Magento\Framework\View\Element\Template;
use Smartosc\Article\Model\ArticleFactory;

class DetailArticle extends Template
{

    public function __construct(Template\Context $context,ArticleFactory $collectionFactory)
    {
        $this->_collectionFactory =$collectionFactory;
        parent::__construct($context);

    }
    public function getArticle(){
        $id = $this->_request->getParam('id');
        $article = $this->_collectionFactory->create();
        $collection =$article->getCollection();
        $detailarticle = $collection->addFieldToFilter('article_id',$id);

        return $detailarticle->getData();
    }

}