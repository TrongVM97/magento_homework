<?php


namespace Smartosc\Article\Block;


use Magento\Framework\View\Element\Template;
use Smartosc\Article\Model\ArticleFactory;

class DisplayAllArticle extends Template
{
    protected $_collectionFactory;

    public function __construct(Template\Context $context,ArticleFactory $collectionFactory)
    {
        $this->_collectionFactory =$collectionFactory;
        parent::__construct($context);

    }
    public function loadAllArticles(){

        $article = $this->_collectionFactory->create();
        $collection =$article->getCollection();
        return $collection->getData();
    }
}