<?php


namespace Smartosc\Article\Model;


use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class Article extends AbstractModel implements IdentityInterface
{

    const CACHE_TAG = 'sm_article';


    protected function _construct()
    {

        $this->_init(\Smartosc\Article\Model\ResourceModel\Article::class);
    }

    /**
     * Return unique ID(s) for each object in system
     *
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}