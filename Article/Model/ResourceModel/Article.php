<?php


namespace Smartosc\Article\Model\ResourceModel;


use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

class Article extends AbstractDb
{

    public function __construct(Context $context, $connectionName = 'article')
    {
        parent::__construct($context, $connectionName);
    }

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
       $this->_init('sm_article','article_id');
    }
}